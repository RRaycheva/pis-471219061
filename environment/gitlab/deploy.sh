#!/bin/bash
set -e
set -x;

export PROJECT_ID=$(gcloud config get-value core/project)

########################################################################################################################
export UNIQUE_IDENTIFIER="471219061"
export NAMESPACE="gitlab-${UNIQUE_IDENTIFIER}"
########################################################################################################################

# NOTE: this doesn't work on MacOS. Instead, please make those changes in the "k8s/gitlab-runner-config.yaml" directly
#sed -i "s/GITLAB_RUNNER_TOKEN/${GITLAB_RUNNER_TOKEN}/g" "k8s/gitlab-runner-config.yaml"
#sed -i "s/PROJECT_ID/${PROJECT_ID}/g" "k8s/gitlab-runner-config.yaml"

# NOTE: Adding unique identifier to the name of k8s deployment and configuration is not required!
#       Since all deployments will have their unique namespaces, configuration and deployment names
#       for different projects will no clash. However it's would make it easier to distinguish
#       configs for different projects. Exception is the gitlab runner configuration in the
#       "k8s/gitlab-runner-config.yaml" file, in which the following should be changed:
#           1. namespace = "gitlab-${UNIQUE_IDENTIFIER}"
#           2. "GOOGLE_APPLICATION_EMAIL=gitlab-runner-${UNIQUE_IDENTIFIER}@tu-sofia-pis-2022-dev.iam.gserviceaccount.com"
#       The two ${UNIQUE_IDENTIFIER} should be replaced with your namespace and service account.
#
#       Those of you who already have working deployments can ignore this.
#sed -i "s/\\\${UNIQUE_IDENTIFIER}/${UNIQUE_IDENTIFIER}/g" k8s/*

kubectl create namespace ${NAMESPACE} || true
kubectl apply -f k8s --namespace ${NAMESPACE}

